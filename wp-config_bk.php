<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'betheme');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('WP_ALLOW_REPAIR', true);
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'LYE,-uny_X<HPERJd&F,GyTVjFM>mC^_mGR1PTi[0$62r`[hY[PV/coAi/vs@2<u');
define('SECURE_AUTH_KEY',  'xI5d.e`xtaA6w@5>UAyb5;sz`^.qL/9)F_+|!-H,`@J}5QmnHh,F:b)2Ufy5 .0D');
define('LOGGED_IN_KEY',    'P<_wIMx4O+62zeC5f@Kzov^aN *_r6_12%q]UBkBBnDl!cctLnvNGm% Jxwlos T');
define('NONCE_KEY',        ' `f*#)&D>gr40;}HhaZ5M/>vVyqT^p16rHDVr$V s%)0z)p`6?^~dtP0$uNtH`9k');
define('AUTH_SALT',        '%4QiEazDAvw&{ QN)IR)]n7x L+f`%}^wNUzY]6E]7Wy !pT%H%9{Ev]o6|.#$6.');
define('SECURE_AUTH_SALT', 'v@(:4&K~slu;)h5ZK>fj#Kc6u6P!klm CPwTLRf`D_Rbu>{uootrxS P*N^s?dg[');
define('LOGGED_IN_SALT',   ' 2Bt]F@ E~d#V:`{0wLfv(Z>aJ([ h#$pZA@,H{wCA)!)A~9r^8Gf& I#n~ *!_v');
define('NONCE_SALT',       '_x:@qMT-U&B`ybDD E-^6(5wG.[P@1LKueEB>e|l`Vlf19z5J&pPP)yB*+.5^gNp');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sn_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
